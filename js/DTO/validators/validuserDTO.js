export default class validuserDTO{

    constructor(
        _validfirstname,
        _validsecondname,
        _validlastname1,
        _validlastname2,
        _validrut,
        _validtype,
        _validaddress,
        _validregion,
        _validcomuna,
        _validemail,
        _validphone
    ){
        this.validfirstname = _validfirstname;
        this.validsecondname = _validsecondname;
        this.validlastname1 = _validlastname1;
        this.validlastname2 = _validlastname2; 
        this.validrut = _validrut; 
        this.validtype = _validtype; 
        this.validaddress = _validaddress; 
        this.validregion = _validregion; 
        this.validcomuna = _validcomuna; 
        this.validemail = _validemail; 
        this.validphone = _validphone;

    }
//creo que hay una mejor manera de hacer esto 
    
    nameeval(){
      if( this.validfirstname.replace(/\s/g, "") ){
          return true;
      }else{
          return false; 
      }
    }

    name2eval(){
        if( this.validsecondname.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }

    lastname1eval(){
        if( this.validlastname1.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }

    }

    lastname2eval(){
        if( this.validlastname2.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }

    }
    //TODO: Ampliar función para validación de RUT
    ruteval(){
        if( this.validrut ){
            if( $.validateRut( this.validrut )){
                return true;
            }else{
                return false;
            }
            
        }else{
            return false; 
        }
    }

    typeeval(){
        if( this.validtype ){
            return true;
        }else{
            return false; 
        }
    }

    addresseval(){
        if( this.validaddress.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
      }

    regioneval(){
        if( this.validregion ){
            return true;
        }else{
            return false; 
        }
    }
    
    comunaeval(){
        if( this.validcomuna ){
            return true;
        }else{
            return false; 
        }
    }

    emaileval(){
        if( this.validemail.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }

    phoneeval(){
        if( this.validphone.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }

    
    // Función para validar un correo o un email. 
    validatecell_email( field, value ){

        switch( field ){
            case 'email':
                let regexemail = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if( regexemail.test( value ) ){
                    console.log('email bueno');
                    return true;   
                }
                else{
                    console.log('email malo');
                    return false;
                }
            case 'cell':
                let regexcell = /^(\+?56)?(\s?)(0?9)(\s?)[98765]\d{7}$/;
                if( regexcell.test( value ) ){
                    return true;   
                }
                else{
                    return false;
                } 
                      
        }
}


    


} 