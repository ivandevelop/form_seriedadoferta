export default class validatebiddingDTO{

    constructor(
              _valididbidding,
              _validdnioffer,
              _validinitdate,
              _validpolicytype,
              _validautomatecal,
              _validnameoffer,
              _validenddate,
              _validamountbidding,
              _validprimacy,
              _validcomment

    ){
        this.valididbidding = _valididbidding;
        this.validdnioffer = _validdnioffer;
        this.validinitdate = _validinitdate;
        this.validpolicytype = _validpolicytype;
        this.validautomatecal = _validautomatecal;
        this.validnameoffer = _validnameoffer;
        this.validenddate = _validenddate;
        this.validamountbidding = _validamountbidding;
        this.validprimacy = _validprimacy;
        this.validcomment = _validcomment;  
    }
    //creo que hay una mejor manera de hacer esto 
    
    idbiddingeval(){
        if( this.valididbidding.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
    dnioffereval(){
        if( this.validdnioffer.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
    initdateeval(){
        if( this.validinitdate.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
  
    }
  
    policytypeeval(){
        if( this.validpolicytype.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
  
    }
      //TODO: Ampliar función para validación de RUT
    automatecaleval(){
        if( this.validautomatecal.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
    nameoffereval(){
        if( this.validnameoffer.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
    enddateeval(){
        if( this.validenddate.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
      }
  
    amountbiddingeval(){
        if( this.validamountbidding.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
    primacyeval(){
        if( this.validprimacy.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }

    commenteval(){
        if( this.validcomment.replace(/\s/g, "") ){
            return true;
        }else{
            return false; 
        }
    }
  
  
      
}