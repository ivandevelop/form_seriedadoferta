export default class payDTO{

    constructor(
                 _idCompany,  
                 _idCountry, 
                 _currency, 
                 _datePayment, 
                 _dni, 
                 _mail,
                 _nameClient,
                 _idPaymentMethod,
                 _status, 
                 _idPage,
                 _total,
                 _token,
                 _url


    ){

                 this.idCompany = _idCompany,  
                 this.idCountry = _idCountry, 
                 this.currency  = _currency, 
                 this.datePayment = _datePayment, 
                 this.dni =  _dni, 
                 this.mail  =  _mail,
                 this.nameClient  =  _nameClient,
                 this.idPaymentMethod = _idPaymentMethod,
                 this.status =  _status, 
                 this.idPage =   _idPage
                 this.total = _total;
                 this.token = _token;
                 this.url = _url; 
    
    }


}