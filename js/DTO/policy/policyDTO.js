export default class policyDTO{
     constructor(
         _codTipoPoliza,
         _tasa,
         _moneda,
         _fecInicial,
         _fecFinal,
         _prima,
         _monto,
         _montoAsegurado,
         _montoAseguradoPesos
     ){
         this.codTipoPoliza = _codTipoPoliza;
         this.tasa = _tasa; 
         this.moneda = _moneda; 
         this.fecInicial = _fecInicial; 
         this.fecFinal = _fecFinal; 
         this.prima = _prima; 
         this.monto = _monto; 
         this.montoAsegurado = _montoAsegurado; 
         this.montoAseguradoPesos = _montoAseguradoPesos;  
     }

}