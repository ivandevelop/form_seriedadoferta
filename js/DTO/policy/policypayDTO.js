export default class policypayDTO{

    constructor(
        _amountGross,
        _currency,
        _datePayment,
        _discount,
        _dni, 
        _idcompany,
        _idCountry, 
        _idPage,
        _idPay, 
        _idPayment){

           this.amountGross = _amountGross;
           this.currency = _currency;
           this.datePayment = _datePayment;
           this.discount = _discount; 
           this.dni = _dni; 
           this.idcompany = _idcompany; 
           this.idCountry = _idCountry;
           this.idPage = _idPage; 
           this.idPay = _idPay;
           this.idPayment = _idPayment;  

    }
}