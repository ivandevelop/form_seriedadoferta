export default class tipoPersonaDTO{

    constructor(
        _codigo,
        _valor,
        _text
    ){
       this.codigo = _codigo;
       this.valor = _valor; 
       this.text = _text; 
    }
}